from hotqueue import HotQueue
import redis
import api
import jobs
import os

#set up variables for redis ip address and port
REDIS_IP = os.environ.get('REDIS_IP')
REDIS_PORT = os.environ.get('REDIS_PORT')

_q = HotQueue("queue", host=REDIS_IP, port=6379, db=1)

#this is a list of the functions in jobs
#the job data dictionary of a given job will identify which function to call and what parameters to calll it with
_commands = [jobs.total_years, jobs.total_bridge, jobs.mean_years, jobs.mean_years_bridge, jobs.graph_for_years, jobs.pie_year, jobs.rolling_mean]


#consumer decorator
@_q.worker
def listen(jid):

    #using api, retrieve data for waiting job from redis
    j_data = api.retrieve_job_data(jid)


    #using api, create an in progress job dictionary and input it to redis
    info = api.in_progress_job_dict(jid)
    api.log_job(info)

    #ensure data was received correctly before continuing
    if(j_data != 'Error'):

        #retrieve command, parameters, and type of job with encoded keys
        #type of job is used later to make sure users retrieve bytes with the correct endpoint
        i = j_data['command'.encode('utf-8')]
        k = j_data['parameters'.encode('utf-8')]
        t = j_data['type'.encode('utf-8')]

        #decode command, parameters, and type
        i = int(i.decode('utf-8'))
        k = k.decode('utf-8')
        t = t.decode('utf-8')

        #call command with parameters
        if(len(k) > 0):
            output = _commands[i](k)
        else:
            output = _commands[i]()
    else:
        output = 'An error occured in logging this job.'

    #using api, update job status and put result in redis
    report = api.complete_job_dict(jid, output, t)
    api.log_job(report)

#start up consumer
listen()
